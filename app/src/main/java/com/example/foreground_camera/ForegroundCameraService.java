package com.example.foreground_camera;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.foreground_camera.App.CHANNEL_ID;

public class ForegroundCameraService extends Service {
    private static final String TAG = "mytag-CameraService";
    private static final int ONGOING_NOTIFICATION_ID = 1566;

    private static final int RECONNECT_CAMERA_AFTER_ERROR_AFTER = 1000;

    private ChatHead mChatHead;
    private boolean mShouldShowPreview = false;
    private CameraManager mCameraManager = null;
    private CameraDevice mCameraDevice = null;
    private Size mPreviewSize = null;
    private ImageReader mImageReader = null;
    private CaptureRequest.Builder mRequestBuilder = null;
    private CameraCaptureSession mCaptureSession = null;

    final Handler handler = new Handler();

    @Override
    public void onCreate() {
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
        startWithPreview();
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy: Service Stopped");
        stopCamera();
        if (mChatHead != null) {
            mChatHead.remove();
            mChatHead = null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mShouldShowPreview = intent.getBooleanExtra("shouldShowPreview", false);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("GR APP")
                .setContentText("detecting crosswalk")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(ONGOING_NOTIFICATION_ID, notification);
        Toast.makeText(this, "onStartCommand\nmShouldShowPreview " + mShouldShowPreview, Toast.LENGTH_SHORT).show();

        // co nen lam ntn k? show/hide preview
        if (mCameraDevice != null) {
            stopCamera();
            if (mChatHead != null) {
                mChatHead.remove();
                mChatHead = null;
            }
            startWithPreview();
        }
        return START_STICKY;
    }

    private void startWithPreview() {
        if (mChatHead == null) mChatHead = new ChatHead(this);
        TextureView textureView = mChatHead.getTextureView();
        if (textureView.isAvailable()) {
            initCam(textureView.getWidth(), textureView.getHeight());
        } else {
            textureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    private void initCam(int width, int height) {
        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        String camId = null;
        try {
            for (String id : mCameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(id);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_BACK) {
                    camId = id;
                    break;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        mPreviewSize = chooseSupportedSize(camId, width, height);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            // repquested in MainActivity
            try {
                Log.e(TAG, "initCam: " + isCameraUsebyApp());
                mCameraManager.registerAvailabilityCallback(mAvailabilityCallback, handler);
                mCameraManager.openCamera(camId, mStateCallback, null);
            } catch (CameraAccessException e) {
                e.printStackTrace();
                Log.e(TAG, "initCam: error");
            }
            return;
        }
    }

    public boolean isCameraUsebyApp() {
        Camera camera = null;
        try {
            camera = Camera.open();
        } catch (RuntimeException e) {
            return true;
        } finally {
            if (camera != null) camera.release();
        }
        return false;
    }

    private CameraManager.AvailabilityCallback mAvailabilityCallback = new CameraManager.AvailabilityCallback() {
        @Override
        public void onCameraAvailable(@NonNull String cameraId) {
            super.onCameraAvailable(cameraId);
            Log.e(TAG, "onCameraAvailable");
            mCameraManager.unregisterAvailabilityCallback(mAvailabilityCallback);
        }

        @Override
        public void onCameraUnavailable(@NonNull String cameraId) {
            super.onCameraUnavailable(cameraId);
            Log.e(TAG, "onCameraUnavailable");
            mCameraManager.unregisterAvailabilityCallback(mAvailabilityCallback);
        }
    };

    private Size chooseSupportedSize(String camId, int textureViewWidth, int textureViewHeight) {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        // Get all supported sizes for TextureView
        CameraCharacteristics characteristics = null;
        try {
            characteristics = manager.getCameraCharacteristics(camId);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        Size[] supportedSizes = map.getOutputSizes(SurfaceTexture.class);

        // We want to find something near the size of our TextureView
        int texViewArea = textureViewWidth * textureViewHeight;
        float texViewAspect = (float) textureViewWidth / textureViewHeight;
        /*
        Size bestSize = supportedSizes[supportedSizes.length - 1];
        float minDiffAspect = 10, minDiffArea = 1000;
        // find most similar aspect and most similar area
        for (Size s : supportedSizes) {
            float aspect, diffAspect;
            if (s.getWidth() < s.getHeight()) aspect = (float) s.getWidth() / s.getHeight();
            else aspect = (float) s.getHeight() / s.getWidth();
            diffAspect = abs(aspect - texViewAspect);
            if (diffAspect < minDiffAspect) {
                minDiffAspect = diffAspect;
                bestSize = s;
            } else if (minDiffAspect == diffAspect) {
                float diffArea = abs(texViewArea - s.getWidth() * s.getHeight());
                if (diffArea < minDiffArea) {
                    minDiffArea = diffArea;
                    bestSize = s;
                }
            }
        }
        */
        return new Size(320, 240);
    }

    private void createCaptureSession() {
        try {
            // Prepare surfaces we want to use in capture session
            ArrayList<Surface> targetSurfaces = new ArrayList();

            // Prepare CaptureRequest that can be used with CameraCaptureSession
            mRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

            if (mShouldShowPreview) {
                SurfaceTexture texture = mChatHead.getTextureView().getSurfaceTexture();
                texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                Surface previewSurface = new Surface(texture);

                targetSurfaces.add(previewSurface);
                mRequestBuilder.addTarget(previewSurface);
            }
            // Configure target surface for background processing (ImageReader)
            mImageReader = ImageReader.newInstance(
                    mPreviewSize.getWidth(), mPreviewSize.getHeight(),
                    ImageFormat.YUV_420_888, 2);
            mImageReader.setOnImageAvailableListener(mImageListener, null);

            targetSurfaces.add(mImageReader.getSurface());
            mRequestBuilder.addTarget(mImageReader.getSurface());

            // Set some additional parameters for the request
            mRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            mRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);

            // Prepare CameraCaptureSession
            mCameraDevice.createCaptureSession(targetSurfaces,
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Now we can start capturing
                                mCaptureSession.setRepeatingRequest(mRequestBuilder.build(), mCaptureCallback, null);
                            } catch (CameraAccessException e) {
                                Log.e(TAG, "createCaptureSession failed");
                            }
//                            mCameraOpenCloseLock.release();
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                            Log.e(TAG, "createCameraPreviewSession failed");
//                            mCameraOpenCloseLock.release();
                        }
                    }, null);
        } catch (CameraAccessException e) {
            Log.e(TAG, "createCaptureSession", e);
        }
    }

    private ImageReader.OnImageAvailableListener mImageListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image image = reader.acquireLatestImage();
            if (image != null) {
                Log.d(TAG, "Got image: " + image.getWidth() + " x " + image.getHeight());
                image.close();
            }
        }
    };

    private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            mCameraDevice = camera;
            Log.e(TAG, "onOpened: ");
            createCaptureSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            camera.close();
            Log.e(TAG, "onDisconnected: ");
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            Map<Integer, String> map = new HashMap<Integer, String>();
            map.put(ERROR_CAMERA_IN_USE, "ERROR_CAMERA_IN_USE");
            map.put(ERROR_MAX_CAMERAS_IN_USE, "ERROR_MAX_CAMERAS_IN_USE");
            map.put(ERROR_CAMERA_DISABLED, "ERROR_CAMERA_DISABLED");
            map.put(ERROR_CAMERA_DEVICE, "ERROR_CAMERA_DEVICE");
            map.put(ERROR_CAMERA_SERVICE, "ERROR_CAMERA_SERVICE");
            Log.e(TAG, "onError: " + map.get(error));

            camera.close();
            mCameraDevice = null;

            if (error == ERROR_CAMERA_IN_USE) {
                // restart camera after ...s
                stopCamera();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e(TAG, ".\n.\n.\n.\n.\n");
                        startWithPreview();
                    }
                }, RECONNECT_CAMERA_AFTER_ERROR_AFTER);
            }
        }
    };


    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            initCam(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };


    private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureResult partialResult) {
            super.onCaptureProgressed(session, request, partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            super.onCaptureCompleted(session, request, result);
        }
    };

    private void stopCamera() {
        try {
            mCaptureSession.close();
            mCaptureSession = null;

            mCameraDevice.close();
            mCameraDevice = null;

            mImageReader.close();
            mImageReader = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
